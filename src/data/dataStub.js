export const dataModel = {
	property: {
		address: {
			firstLine: "Eddington House",
			secondLine: "16 Wigginton Road",
			city: "Tamworth",
			postCode: "B79 8PB",
		},
		value: 189950
	},
	buyer: {
		name: "Mr David Shepherd",
		buyingPos: "Property to sell - 20 weeks on the market",
		sellingPos: "Mortgage required - approved",
		timeScale: "Would like to move in 10 weeks - no chain",
		offer: {
			price: 179000,
			date: "24/10/2013",
			time: "20.17"
		}
	},
	response: {
		accept: false,
		date: "24/10/2013",
		time: "21.05",
		counterOffer: ""
	}
};