import React, {PropTypes} from 'react';

import './buyerDetails.scss';

class BuyerDetails extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      boxOpen: true,
      boxClass: 'active'
    }
  }

  toggleBox() {
    this.setState({boxOpen: !this.state.boxOpen}, ()=>{
      if(this.state.boxOpen){
        this.setState({boxClass: 'active'})
      }else{
        this.setState({boxClass: ''})
      }
    });
  }

  render() {
    if (!this.props.dataModel)
      return null;

    let message = this.state.boxOpen ? 'Hide' : 'Show'
  
    return (
      <div className={`box-container buyer-details ${this.state.boxClass}`}>
        <div className="box-container__toggle" onClick={this.toggleBox.bind(this)}>{message}</div>
        <h2>Buyer's details</h2>
        <p><span>Name:</span>{this.props.dataModel.name}</p>
        <p><span>Buying position:</span>{this.props.dataModel.buyingPos}</p> 
        <p><span>Financial position:</span>{this.props.dataModel.sellingPos}</p> 
        <p><span>Timescale:</span>{this.props.dataModel.timeScale}</p> 
      </div>
    );
  }
}

BuyerDetails.propTypes = {
  dataModel: PropTypes.object
};

export default BuyerDetails;
