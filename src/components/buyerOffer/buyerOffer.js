import React, {PropTypes} from 'react';

import './buyerOffer.scss';

class BuyerOffer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      boxOpen: true,
      boxClass: 'active'
    }
  }

  toggleBox() {
    this.setState({boxOpen: !this.state.boxOpen}, ()=>{
      if(this.state.boxOpen){
        this.setState({boxClass: 'active'})
      }else{
        this.setState({boxClass: ''})
      }
    });
  }

  render() {
    if (!this.props.dataModel)
      return null;

    let message = this.state.boxOpen ? 'Hide' : 'Show'
  
    return (
      <div className={`box-container buyer-offer ${this.state.boxClass}`}>
        <div className="box-container__toggle" onClick={this.toggleBox.bind(this)}>{message}</div>
        <h2>Buyer's offer</h2>
        <p className="buyer-offer__price">{`£${this.props.dataModel.offer.price}`}</p>
        <span>Subject to offer qualification by Purplebricks</span>
        <div className="row">
          <div className="col-md-6 col--vcenter">
            <button className="btn btn-default">View comments</button>
          </div>
          <div className="col-md-6 col--vcenter">
            <p className="timestamp">Date: {this.props.dataModel.offer.date}</p>
            <p className="timestamp">Time: {this.props.dataModel.offer.time}</p>
          </div>
        </div>
      </div>
    );
  }
}

BuyerOffer.propTypes = {
  dataModel: PropTypes.object
};

export default BuyerOffer;
