import React, {PropTypes} from 'react';

import './sellerResponse.scss';

class SellerResponse extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      boxOpen: true,
      boxClass: 'active'
    }
  }

  toggleBox() {
    this.setState({boxOpen: !this.state.boxOpen}, ()=>{
      if(this.state.boxOpen){
        this.setState({boxClass: 'active'})
      }else{
        this.setState({boxClass: ''})
      }
    });
  }

  render() {
    if (!this.props.dataModel)
      return null;

    let message = this.state.boxOpen ? 'Hide' : 'Show'
  
    if(!this.props.dataModel.accept)
      return (
        <div className={`box-container seller-response seller-response--refuse ${this.state.boxClass}`}>
          <div className="box-container__toggle" onClick={this.toggleBox.bind(this)}>{message}</div>
          <h2>Your response</h2>
          <p className="seller-response__message">You have withdrawn your acceptance of the offer</p>
          <div className="row">
            <div className="col-md-6 col--vcenter">
              <button className="btn btn-default">View comments</button>
            </div>
            <div className="col-md-6 col--vcenter">
              <p className="timestamp">Date: {this.props.dataModel.date}</p>
              <p className="timestamp">Time: {this.props.dataModel.time}</p>
            </div>
          </div>
        </div>
      );
  }
}

SellerResponse.propTypes = {
  dataModel: PropTypes.object
};

export default SellerResponse;
