import React, {PropTypes} from 'react';
import PropertyDetails from './propertyDetails/propertyDetails';
import BuyerDetails from './buyerDetails/buyerDetails';
import BuyerOffer from './buyerOffer/buyerOffer';
import SellerResponse from './sellerResponse/sellerResponse';
import Negotiation from './negotiation/negotiation';

class IndexPage extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  componentWillMount() {
    this.props.fetchDataModel();
  }

  processOffer(offer){
    this.returnObj = {
      returnOffer: parseFloat(offer.replace(/,/g, ''))
    }
    console.log(this.returnObj);
  }
  
  render() {
    return (
      <div className="container">
        <PropertyDetails dataModel={this.props.dataModel.property} />
        <BuyerDetails dataModel={this.props.dataModel.buyer} />
        <div className="row">
          <div className="col-sm-6">
            <BuyerOffer dataModel={this.props.dataModel.buyer} />
          </div>
          <div className="col-sm-6">
            <SellerResponse dataModel={this.props.dataModel.response} />
          </div>
        </div>
        <Negotiation submit={this.processOffer.bind(this)} />
      </div>
    );
  }
}

IndexPage.propTypes = {
  fetchDataModel: PropTypes.func.isRequired,
  dataModel: PropTypes.object.isRequired
};

export default IndexPage;
