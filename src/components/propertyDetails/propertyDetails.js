import React, {PropTypes} from 'react';

import './propertyDetails.scss';

class PropertyDetails extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      boxOpen: true,
      boxClass: 'active'
    }
  }

  toggleBox() {
    this.setState({boxOpen: !this.state.boxOpen}, ()=>{
      if(this.state.boxOpen){
        this.setState({boxClass: 'active'})
      }else{
        this.setState({boxClass: ''})
      }
    });
  }

  render() {
    if (!this.props.dataModel)
      return null;

    let message = this.state.boxOpen ? 'Hide' : 'Show'
  
    return (
      <div className={`box-container box-container--purple property-details ${this.state.boxClass}`}>
        <div className="row">
          <div className="col-sm-12">
            <h2>The Property</h2>
          </div>
          <div className="col-sm-8 property-details__detail">
            <img src="http://placehold.it/120x80" />
            <p>{`${this.props.dataModel.address.firstLine}, ${this.props.dataModel.address.secondLine}, ${this.props.dataModel.address.city}, ${this.props.dataModel.address.postCode}`}</p>
          </div>
          <div className="col-sm-4 property-details__value">
            <p className="unemph"><span>Asking price:</span></p>
            <p>{this.props.dataModel.value}</p>
            <button className="btn btn-default">View extras included</button>
          </div>
        </div>
        <div className="box-container__toggle" onClick={this.toggleBox.bind(this)}>{message}</div>
      </div>
    );
  }
}

PropertyDetails.propTypes = {
  dataModel: PropTypes.object
};

export default PropertyDetails;
