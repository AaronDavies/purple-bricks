import React, {PropTypes} from 'react';

import './negotiation.scss';

class Negotiation extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      offer: '',
      boxOpen: true,
      boxClass: 'active'
    }
  }

  submitOffer(){
    this.props.submit(this.state.offer);
  }

  handleInput(event){
    let input = event.target.value;

    input = parseInt(input.replace(/,/g, ''))
    
    if(/^-?\d*\.?\,?\d*$/.test(input) && !isNaN(input)){   
      input = input.toLocaleString();
      this.setState({offer: input})
    }else{
      this.setState({offer: ''})
    }    
  }

  toggleBox() {
    this.setState({boxOpen: !this.state.boxOpen}, ()=>{
      if(this.state.boxOpen){
        this.setState({boxClass: 'active'})
      }else{
        this.setState({boxClass: ''})
      }
    });
  }

  render() {
    let message = this.state.boxOpen ? 'Hide' : 'Show'

    return (
      <div className={`box-container negotiation ${this.state.boxClass}`}>
        <div className="box-container__toggle" onClick={this.toggleBox.bind(this)}>{message}</div>
        <h2>Re-open negotiations</h2>
        <div className="negotiation__expert">
          <div className="row">
            <div className="col-sm-8 col--vcenter">
              <p>Would you like your Expert, Robert White, to negotiate on your behalf? It’s completely free!</p>
            </div>
            <div className="col-sm-4 col--vcenter">
              <button className="btn btn--purple btn-default">Negotiate for me</button>
            </div>
          </div>
        </div>
        <h3>Enter an offer you would be willing to accept:</h3>
        <div className="row negotiation__row">
          <div className="col-sm-8 col--vcenter negotiation__price">
            <span>£</span><input type="text" pattern="[0-9]+([,][0-9]{1,2})?" placeholder="Enter a price" className="form-control" value={this.state.offer} onChange={this.handleInput.bind(this)}/>
          </div>
          <div className="col-sm-4 col--vcenter negotiation__submit">
            <button className="btn btn--purple btn-default" type="submit" onClick={this.submitOffer.bind(this)}>Submit offer</button>
          </div>
        </div>
        <button className="btn btn--purple btn-default">Add comments</button>
      </div>
    );
  }
}

Negotiation.propTypes = {
  dataModel: PropTypes.object
};

export default Negotiation;
