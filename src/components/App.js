import React, { PropTypes } from 'react';

class App extends React.Component {
  render() {
    return (
      <div>
        <header>
          <div className="header">
            <div className="container">
              <h1>Secure negotiation centre</h1>
            </div>
          </div>
        </header>
        <main>
          {this.props.children}
        </main>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default App;
