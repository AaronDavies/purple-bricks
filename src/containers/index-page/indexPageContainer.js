import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../actions/indexActions';
import IndexPage from '../../components/IndexPage';

export const IndexPageContainer = (props) => {
  return (
    <IndexPage
      fetchDataModel={props.fetchDataModel}
      dataModel={props.dataModel}
    />
  );
};

IndexPageContainer.propTypes = {
  fetchDataModel: PropTypes.func.isRequired,
  dataModel: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    dataModel: state.indexData.data
  };
}

function mapDispatchToProps(dispatch) {
  return {
    fetchDataModel: () => {
      dispatch(actions.fetchDataModel());
    }
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(IndexPageContainer);
