import { combineReducers } from 'redux';
import indexData from './indexReducer';
import {routerReducer} from 'react-router-redux';

const rootReducer = combineReducers({
  indexData,
  routing: routerReducer
});

export default rootReducer;
